---
title: 日程表
date: 2017-10-15
type: "schedule"
comments: false
---

# 学习中
- Spring Cloud 微服务解决方案
- Kubernetes 基于容器技术的分布式架构
- Vertx java框架

# 计划
- Mycat 数据库分库分表中间件

<!-- more -->

# 已学习/尝试使用
- Grafana 多数据源数据分析
- Mattermost 开源的slack，企业内部消息平台
- Vue&Angular WEB MVVM框架
- GitLab-CI 持续集成工具
- Docker 容器技术
- Docsify 静态文档工具
- JMeter 压力测试工具
- TICK技术栈 数据采集、存储、分析、监控方案

# 归档
- Jenkins 持续集成工具（目前最主流的CI工具）
- Gitbook 静态文档工具，相对的阅读体验更佳，但配置与UI较差
- Guava Google的java工具类
- Dart 类js语言

# 备注
- 学习中： 目前正在学习的，或者长期跟进学习的内容
- 计划: 一些不错的技术，记录，避免忘记
- 已学习/尝试使用: 记录学习过的技术，持续关注
- 归档: 对于我来说老旧/重复/感兴趣但较长时间不会去学习的技术点，已经用其他技术代替，但该技术仍然十分重要，所以仍然关注